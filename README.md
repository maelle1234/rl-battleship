# RL-battleship

Ce projet est réalisé dans le cadre du cours d'introduction à l'apprentissage par renforcement de 3ème année à l'ENSAI. Il consiste en l'implémentation d'une version simplifiée d'un jeu de bataille navale, avec un algorithme de QLearning qui doit trouver comment jouer de manière optimale.

## Bataille navale

L'objectif de la bataille navale est de couler tous les bateaux de son adversaire, qui sont placés sur une grille. Plusieurs variantes existent mais nous choisissons ici de ne travailler qu'avec des grilles carrées et nous décidons également que le premier bateau placé est de taille 1, le deuxième de taille 2 et ainsi de suite. Dans notre contexte, il n'y a pas d'adversaire: chaque grille de jeu est générée aléatoirement par le programme en fonction de la taille de grille souhaitée et le nombre de bateaux à positionner.

## Organisation du code

- `board` comprend les classes représentant les grilles de jeu. Nous avons fait la distinction entre la grille de jeu, inconnue du joueur, et la représentation de la grille qu'a le joueur au cours du jeu. Plus de détails sont disponibles dans la documentation de `AbstractBoard`, `PlayerBoard` et `TrueBoard`. Un bateau est représenté par un 1, et pas de bateau par 0. 
- `game` comprend les classes représentant une partie de jeu. Une partie est composée principalement d'une grille, générée aléatoirement, et d'un joueur. 
- `player` comprend les classes représentant les types de joueurs. `RandomPlayer` est un joueur qui joue de manière complètement aléatoire, et `QLearningPlayer` est un joueur qui joue selon une stratégie de QLearning. 

Le script pour lancer une partie est dans le fichier `main.py`.

## Contributrices

- Maryem Ben Hamouda
- Maëlle Cosson


