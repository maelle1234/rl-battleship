from game.gameRandom import GameRandom
from game.Gamelearning import gameLearning
from player.qLearningPlayer import QLearningPlayer
from player.randomPlayer import RandomPlayer

if __name__ == '__main__':
    board_size = 5
    n_boats = 1
    n_games = 10
    player1 =QLearningPlayer(board_size=board_size, nboats=n_boats, epsilon=0.1, alpha=0.5, gamma=1)
     # Jeu d'un certain nombre de parties pour entraîner le joueur
    for i in range(n_games):
        game = gameLearning(player1, board_size=board_size, nboats=n_boats)
        game.play()

    # Après l'entraînement, jouer une partie en utilisant la stratégie apprise
    game = gameLearning(player1, board_size=board_size, nboats=n_boats)
    game.play()
    player2 = RandomPlayer()
    myGame = GameRandom(player2, nboats=2)
    #myGame.player_board.display_board()
    myGame.true_board.display_board()
    '''
    print(myGame.is_game_over())
    myGame.play_one_turn((0,0))
    myGame.player_board.display_board()
    myGame.play_one_turn((3,2))
    myGame.player_board.display_board()
    print(myGame.n_turns)
    '''
    myGame.play()
  
  
