import numpy as np 
from math import comb

class QLearningPlayer:
    
    def __init__(self, board_size, nboats, epsilon, alpha, gamma):
        self.board_size = board_size # taille de la grille de jeu
        self.nboats = nboats # nombre de bateaux
        self.epsilon = epsilon # taux d'exploration
        self.alpha = alpha # taux d'apprentissage
        self.gamma = gamma # facteur d'actualisation
        self.q_table = np.zeros((board_size*board_size, 2*nboats)) # initialisation de la table Q à zéro
        self.last_state = None # dernier état de jeu
        self.last_action = None # dernière action effectuée
    
    def get_move(self, player_board):
        '''
        Returns the position of the cell to play according to the Q-learning strategy.

        Parameters
        ----------
        player_board : PlayerBoard
            given board from the player's perspective

        Returns
        -------
        tuple
            coordinates of the chosen cell
        '''

        # If no empty cells left, raise an exception
        if not 2 in player_board.board:
            raise ValueError("There are no empty cells left to play!")

        if np.random.uniform(0, 1) < self.epsilon:
            # Select a random unplayed cell
            move = player_board.get_random_unkown_cell()
        else:
            # Select the cell with the highest Q-value
            possible_moves = []
            q_values = []
            for i in range(player_board.size):
                for j in range(player_board.size):
                    if player_board.get_cell_value((i, j)) == 2:
                        possible_moves.append((i, j))
                        q_values.append(self.q_table[i, j, 0] + self.q_table[i, j, 1])

            if len(possible_moves) == 0:
                raise ValueError("There are no empty cells left to play!")

            move_idx = np.argmax(q_values)
            move = possible_moves[move_idx]

        return move
    
    def update_q_values(self, reward, next_state):
        current_index = np.ravel_multi_index(np.where(self.last_state == 1), (self.board_size, self.board_size)) # récupération de l'indice de l'état actuel
        next_index = np.ravel_multi_index(np.where(next_state == 1), (self.board_size, self.board_size)) # récupération de l'indice de l'état suivant
        self.q_table[current_index, self.last_action] += self.alpha * (reward + self.gamma * np.max(self.q_table[next_index, :]) - self.q_table[current_index, self.last_action]) # mise à jour de la Q-valeur correspondant à l'action effectuée dans l'état actuel
        
    def game_reset(self):
        self.last_state = None # réinitialisation de l'état précédent
        self.last_action = None # réinitialisation de la dernière action effectuée
