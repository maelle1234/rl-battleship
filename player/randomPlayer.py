

class RandomPlayer:
    # on pourrait probablement faire une classe abstraite Player

    def get_move(self, player_board):
        '''
        A random player, given a board, will just randomly select a cell that has not been played yet.

        Parameters
        ----------
        player_board : PlayerBoard
            given board from the player's perspective
        
        Return
        ------
        tuple
            coordinates of the randomly selected cell to play
        '''
        return player_board.get_random_unkown_cell()
    