import numpy as np
from random import randint

from board.abstractBoard import Board

class playerBoard(Board):

    '''
    Represents a battleship board from the player's perspective.
    0 means the cell is empty (no ship), 1 means there is a ship on this cell and 2 means
    the player hasn't tried this cell yet (unkown status).
    At the beginning of the game, the grid is full of 2's and at each trial the value of
    a cell is changed from 2 to 0 or 1.

    Parameters
    ----------
    size : int
        size of the grid (grid is square)

    Attributes
    ----------
    size : int
        size of the grid (grid is square)
    board : np.array
        matrix representation of the board
    row_labels : list
        labels of the rows, labeled as integers
    col_labels : list
        labels of the columns, labeled as uppercase letters 
    '''

    def __init__(self, size):
        super().__init__(size)
        self.board = np.full((size, size), 2, dtype=np.int8) 
        self.last_played_cell = None
        self.last_played_cell_status = None

    def change_cell_status(self, cell, new):
        '''
        Changes the status of a given cell after verifying it exists.

        Parameters
        ----------
        cell : tuple
            coordinates of the given cell
        new : int
            value to be changed for
            1 if a ship has been hit, 0 if there is no ship
        '''


        if cell[0] < 0 or cell[0] > self.size - 1 or  cell[1] < 0 or cell[1] > self.size - 1:
            raise ValueError("cell should belong to the grid")
        if not new in [0, 1]:
            raise ValueError("new should be 0 or 1")

        self.board[cell[0], cell[1]] = new
        self.last_played_cell = cell
        self.last_played_cell_status = new

    def get_random_unkown_cell(self):
        '''
        Returns location of a random cell that hasn't been played yet (unkown).

        Returns
        -------
        tuple
            Coordinates of the chosen cell
        '''
        all_unkown_cells = np.transpose((self.board == 2).nonzero())
        random_cell_idx = randint(0, len(all_unkown_cells)-1)
        random_cell = all_unkown_cells[random_cell_idx]
        return (random_cell[0], random_cell[1])
    def get_cell_status(self, cell):
        '''
        Returns the status of a given cell after verifying it exists.

        Parameters
        ----------
        cell : tuple
            coordinates of the given cell

        Returns
        -------
        int
            0 if there is no ship, 1 if a ship has been hit, 2 if the cell hasn't been tried yet
        '''
        if cell[0] < 0 or cell[0] > self.size - 1 or cell[1] < 0 or cell[1] > self.size - 1:
            raise ValueError("cell should belong to the grid")
        return self.board[cell[0], cell[1]]


if __name__ == '__main__':

    myboard = playerBoard(5)
    myboard.display_board()
    print(myboard.get_cell_value((1, 1)))
    myboard.change_cell_status((2,3), 1)
    myboard.change_cell_status((2,2), 0)
    myboard.display_board()
    print(myboard.get_random_unkown_cell())