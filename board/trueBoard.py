import numpy as np
import random

from board.abstractBoard import Board

class trueBoard(Board):

    '''
    Represents a battleship board against which the player plays.
    0 means the cell is empty (no ship), 1 means there is a ship on this cell.

    Parameters
    ----------
    size : int
        size of the grid (grid is square)

    Attributes
    ----------
    size : int
        size of the grid (grid is square)
    board : np.array
        matrix representation of the board
    row_labels : list
        labels of the rows, labeled as integers
    col_labels : list
        labels of the columns, labeled as uppercase letters 
    '''

    def __init__(self, size):
        super().__init__(size)
        self.board = np.zeros((size, size), dtype=np.int8)


    def add_boat(self, boat_size, position, direction):
        '''
        Updates the board to add a ship after verifying the ship can
        be added under the given parameters and state of the board.

        Parameters
        ----------
        boat_size : int
            size of the boat to be added
        position : tuple
            top left coordinates of the position where the boat should be located
        direction : string
            'H' if the boat should be horizontal
            'V' if the boat should be vertical
        '''

        # verif des paramètres passés en entrée
        if not direction.upper() in ['H', 'V']:
            raise ValueError("direction should be 'H' for horizontal and 'V' for vertical")
        if boat_size < 1:
            raise ValueError("boat_size should be at least 1")
        if position[0] < 0 or position[0] > self.size - 1 or  position[1] < 0 or position[1] > self.size - 1:
            raise ValueError("position should belong to the grid")
        
        # verif de la cohérence avec la taille globale de la grille
        if (direction == 'H' and position[1] + boat_size > self.size) or (direction == 'V' and position[0] + boat_size > self.size):
            raise ValueError("boat_size is too large for the given position and direction")
        
        updated_board = self.board.copy()
        
        if direction == 'H':
            for i in range(boat_size):
                if self.board[position[0], position[1]+i] == 1: # on vérifie qu'il n'y ait pas déjà un bateau
                    raise ValueError("there is already a ship!")
                else:
                    updated_board[position[0], position[1]+i] = 1
        
        if direction == 'V':
            for i in range(boat_size):
                if self.board[position[0]+i, position[1]] == 1: # on vérifie qu'il n'y ait pas déjà un bateau
                    raise ValueError("there is already a ship!")
                else:
                    updated_board[position[0]+i, position[1]] = 1
        
        self.board = updated_board

        ### idée d'amélioration mais peut-être pas important pour le but ici: représenter chaque bateau par un 
        ### numéro unique sur la grille (1, 2, 3) ...
        # add the ship to the list of ships
        # self.ships.append((boat_size, position, direction))


    def generate_random_board(self, nboats):
        '''
        Updates the board to fill it randomly with the specified number of boats. 
        It is assumed that the first boat should be of size 1, the second of size 2 and so on.

        Parameters
        ----------
        nboats : int
            number of boats to play with
        '''
        # on part du principe que s'il y a 5 bateaux, ils sont de taille 1 à 5
        if nboats < 1:
            raise ValueError('nboats should be at least 1')
        
        for boat_size in range(1, nboats+1):
            placed = False
            while not placed:
                try:
                    pos = (random.randint(0,self.size-1), random.randint(0,self.size-1))
                    dir = random.choice(('H', 'V'))
                    print(pos, dir)
                    self.add_boat(boat_size, pos, dir)
                except ValueError:
                    pass
                else:
                    placed = True

    def is_game_over(self):
        ''' Checks if the game is over. The game is over if all the ships have been sunk.

        Returns
        -------
        bool
            True if the game is over, False otherwise'''
   
      
     
        for i in range(self.size):
            for j in range(self.size):
                if self.board[i][j] == 1:
                    return False

        return True

    
if __name__ == '__main__':

    myboard = trueBoard(5)
    myboard.display_board()
    #myboard.add_boat(4,(0,0),'V')
    #myboard.add_boat(2,(0,0),'V')
    myboard.generate_random_board(3)
    myboard.display_board()