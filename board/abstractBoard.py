import numpy as np
import pandas as pd
from abc import ABC

class Board(ABC):

    '''
    Represents a battleship board.

    Parameters
    ----------
    size : int
        size of the grid (grid is square)

    Attributes
    ----------
    size : int
        size of the grid (grid is square)
    board : np.array
        matrix representation of the board
    row_labels : list
        labels of the rows, labeled as integers
    col_labels : list
        labels of the columns, labeled as integers
    '''

    def __init__(self, size):
        self.size = size
        self.row_labels = range(size)
        self.col_labels = range(size)
        self.board = np.empty((size, size), dtype=np.int8)
            

    def display_board(self):
        '''
        Displays the current board.
        '''
        pd_board = pd.DataFrame(self.board, index=self.row_labels, columns=self.col_labels)
        print(pd_board)

    def get_cell_value(self, cell):
        if cell[0] < 0 or cell[0] > self.size - 1 or  cell[1] < 0 or cell[1] > self.size - 1:
            raise ValueError("cell should belong to the grid")
        else:
            return self.board[cell]
        