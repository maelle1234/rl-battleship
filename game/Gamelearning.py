import numpy as np
import sys
sys.path.append('.')

from board.trueBoard import trueBoard
from board.playerBoardlearning import playerBoard
from player.qLearningPlayer import QLearningPlayer


class gameLearning:

    def __init__(self, player, board_size=5, nboats=1):
        self.nboats = nboats
        self.player = player
        self.true_board = trueBoard(board_size)
        self.true_board.generate_random_board(nboats)
        self.player_board = playerBoard(board_size)
        self.n_turns = 0
        self.is_training = isinstance(self.player, QLearningPlayer)


    def is_game_over(self):
        '''
        Returns True if all ships have been identified, False otherwise.
        '''
        res = np.array_equal(np.where(self.player_board.board == 1), np.where(self.true_board.board == 1))
        if res:
            print('Congratulations! You won the game in {} turns'.format(self.n_turns))
        return res

    def play_one_turn(self, cell):
        '''
        At each turn, the player inputs a cell to play in. 
        If a boat is in this cell, it's a win.
        In any case, the player board is being updated with the newly obtained information.

        Parameters
        ----------
        cell : tuple
            coordinates of the cell that should be played
        '''
        print('Playing cell: {}'.format(cell))
        true_cell_val = self.true_board.get_cell_value(cell)
        if true_cell_val == 0:
            print('MISS - there is no ship on this cell!')
            reward = -1
        elif true_cell_val == 1:
            print('WIN - there is a ship on this cell!')
            reward = 100
        else:
            print('This should never happen')
            reward = 0
        self.player_board.change_cell_status(cell, true_cell_val)
        self.n_turns += 1
        if self.is_training:
            self.player.update_q_values(reward, self.player_board.last_played_cell)
        return reward  # retourner la valeur de la récompense


    def play(self):
            cell_to_play = self.player.get_move(self.player_board)
            while not self.is_game_over():
                print('Turn {} --------------------'.format(self.n_turns))
                print('Player\'s board:')
                self.player_board.display_board()
                reward = self.play_one_turn(cell_to_play)
                cell_to_play = self.player.get_move(self.player_board)
                self.player_board.display_board()  # afficher la grille du joueur après chaque tour
                if self.is_training:
                    self.player.last_state = self.player_board.last_played_cell
                    self.player.last_action = self.player_board.last_played_cell_status - 1
                    self.player.update_q_values(reward, self.player_board.last_played_cell)




if __name__ == '__main__':
    board_size = 5
    n_boats = 1
    n_games = 10

    # Création d'un joueur Q-learning avec les hyperparamètres spécifiés
    player = QLearningPlayer(board_size=board_size, nboats=n_boats, epsilon=0.1, alpha=0.5, gamma=1)

    # Jeu d'un certain nombre de parties pour entraîner le joueur
    for i in range(n_games):
        game = game(player, board_size=board_size, nboats=n_boats)
        game.play()

    # Après l'entraînement, jouer une partie en utilisant la stratégie apprise
    game = game(player, board_size=board_size, nboats=n_boats)
    game.play()
