import numpy as np
import sys
sys.path.append('.')

from board.trueBoard import trueBoard
from board.playerBoardRandom import playerBoard
from player.randomPlayer import RandomPlayer


class Game:

    def __init__(self, player, board_size = 5, nboats = 1):
        self.nboats = nboats
        self.player = player
        self.true_board = trueBoard(board_size)
        self.true_board.generate_random_board(nboats)
        self.player_board = playerBoard(board_size)
        self.n_turns = 0


    def is_game_over(self):
        '''
        Returns True if all ships have been identified, False otherwise.
        '''
        res = np.array_equal(np.where(self.player_board.board == 1), np.where(self.true_board.board == 1))
        if res:
            print('Congratulations! You won the game in {} turns'.format(self.n_turns))
        return res


    def play_one_turn(self, cell):
        '''
        At each turn, the player inputs a cell to play in. 
        If a boat is in this cell, it's a win.
        In any case, the player board is being updated with the newly obtained information.

        Parameters
        ----------
        cell : tuple
            coordinates of the cell that should be played
        '''
        print('Playing cell: {}'.format(cell))
        true_cell_val = self.true_board.get_cell_value(cell)
        if true_cell_val == 0:
            print('MISS - there is no ship on this cell!')
        elif true_cell_val == 1:
            print('WIN - there is a ship on this cell!')
        else:
            print('This should never happen')
        self.player_board.change_cell_status(cell, true_cell_val)
        self.n_turns += 1


    def play(self):
        while not self.is_game_over():
            print('Turn {} --------------------'.format(self.n_turns))
            print('Player\'s board:')
            self.player_board.display_board()
            cell_to_play = self.player.get_move(self.player_board) # TODO
            self.play_one_turn(cell_to_play)


if __name__ == '__main__':

    myRandomPlayer = RandomPlayer()
    myGame = Game(myRandomPlayer, nboats=2)
    #myGame.player_board.display_board()
    myGame.true_board.display_board()
    '''
    print(myGame.is_game_over())
    myGame.play_one_turn((0,0))
    myGame.player_board.display_board()
    myGame.play_one_turn((3,2))
    myGame.player_board.display_board()
    print(myGame.n_turns)
    '''
    myGame.play()
